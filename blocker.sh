# Generate 256 bit random number for key and write it to OTP. Raspberry Pi specific
rpi-otp-private-key -w $(openssl rand -hex 32)
# Read the key. Raspberry Pi specific
rpi-otp-private-key -b > key.bin
# Commands past this point are standard cryptsetup
# Create an encrypted FS e.g. on partition 3 of an SD card
BLK_DEV=/dev/mmcblk0p3
sudo apt install cryptsetup
sudo cryptsetup luksFormat --key-file=key.bin --key-size=256 --type=luks2 ${BLK_DEV}
# For debug
sudo cryptsetup luksDump ${BLK_DEV}
# Map the encrypted block device and create a file-system
sudo cryptsetup luksOpen ${BLK_DEV} encrypted-disk --key-file=./key.bin
sudo mkfs /dev/mapper/encrypted-disk
mkdir -p crypto-fs
sudo mount /dev/mapper/encrypted-disk crypto-fs
sudo sh -c 'echo "secret sauce" > crypto-fs/recipes.txt'